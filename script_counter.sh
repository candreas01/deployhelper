artifactory_url="http://localhost:8081/artifactory"
echo "artifactory_url: " $artifactory_url
repo="libs-release-local"
echo "repo " $repo
artifacts="com/qaagility/CounterWebApp"
echo "artifacts " $artifacts
name=$artifact
url=$artifactory_url/$repo/$artifacts
echo $url
file=`curl -s -u admin:password $url/maven-metadata.xml`
version=`curl -s -u admin:password $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`
echo version $version
build=`curl -s -u admin:password $url/$version/maven-metadata.xml | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`
echo build $build
BUILD_LATEST="$url/$version/CounterWebApp-$version.war"
echo "File Name  = " $BUILD_LATEST
echo $BUILD_LATEST > filename.txt
